---
title: "License"
date: 2024-06-16T17:53:13+02:00
description: "DOG is GPL v2"
Author: Wolf Bergenheim
aliases: ["install", "install-dog"]
---

DOG is distributed under the
[GNU GPL (version 2)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html#SEC1).

In short it gives you the right to distribute and modify copies of
DOG, but you must not change the actual licence it is distributed
under.
