---
title: "Build DOG form source"
date: 2024-06-19T07:11:04+02:00
description: "A guide for contributors"
Author: Wolf Bergenheim
tags: [source]
categories: [developer]
---

# Prerequisites

First and only dependency is Borland C++ 3.1. Why this compiler and version? It's
simply because that's what I had when I first started developing `DOG`. Also I
quite like Borland software nowadays, though I didn't have any back in the
90s. :)

Older version of `DOG` could be built with e.g. Turbo C 2.01, however it's no
longer true since I moved to a Makefile based build mechanism.  Currently there
are no plans to support Turbo C in the future. Instead I will work on supporting
OpenWatcom or IA-16 GCC in the future.

# Building

`DOG` source tree has this structure:

```
dog
├── bin
├── doc
└── src
    ├── ext
    └── util
```

## /dog/

The *dog* directory contains the base Makefile. This Makefile is mainly used to
build the release versions of `DOG` with the `bindist` and `srcdist` targets. For
development you will probably want to build from the src directory.

## /dog/bin/

The *dog/bin* directory is where the binaries are copied to when you use the
`install` target. It also contains a few sample dogfiles. This directory is
what's the main content of the binary release zip file.

## /dog/src/

The *dog/doc* directory contains just som documentation.

The *dog/src* directory is where you can find source code of `DOG.COM`. The
source started out as a single `dog.c` file, and then I broke out some other
parts, but used an `#include` to still have all the source in a single file from
the compilers point of view. After migrating to make, each `*.c` file is built
into an object file and then they are all linked together to make `DOG.COM`.

The Makefile in this directory has several useful targets and you can also use
some defines to enable full or partial debuggin printfs. The main target is
naturally `dog`, other words just type `make dog` on the command line to build
the `DOG.COM` binary. You can also build the external commands and utilities by
using the `ext` and `util` targets.

The `all` target target will build `ext`, `util` and `dog`. To build an object
file you can simply build the obj target e.g. `make alse.obj`. This can be handy
in case you want to build one component with debug defines.

These debug defines are pre-defined in the Makefile, you can add more to meet
your needs:

*  `-DDEBUG` - This enables all debug
*  `-DED` - Enables `ENV_DEBUG`
*  `-DOG` - Enables `DOG_DEBUG`
*  `-DBAT` - Enables `BAT_DEBUG`

Additionally there is `-DWARN` to build with all warnings on treated as errors.

## /dog/src/ext/

The *dog/src/ext* directory contains all the external commands. Each external
command is self-contained in a single source file. However I'm considering
merging common functions into an ext library. But for now it doesn't exist. To
build a command simply use the command as a target: e.g. `make ls` will build
`ls.c` to `LS.COM`.

## /dog/src/util/

The *dog/src/util* directory is similar to the *dog/src/ext* directory in that
each command is its own C file. The programs however are mostly used for
developing and testing DOG, and are not really of use to end users. See the
Makefile for the targets, or simply use `make all`
