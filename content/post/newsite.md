---
title: "New Site"
date: 2024-05-30T07:29:45+02:00
description: "DOG gets a new website"
Author: Wolf Bergenheim
tags: []
categories: []
---

After about 22 years I decided to pick up DOG again, and noticed that
the old site had broken. So I decided that rather than resurrect the
pretty bare-bones old site, I decided to roll out a new one built with
[hugo](https://gohugo.io/). This means it's a static website, but
GitHub offers the dynamic functionality I had implemented on my
own. Hugo gives a way to post small additional articles like this in a
convenient way too! :)

So welcome to the new DOG Operating Guide site!

On the top you'll find links to:

*  [About](/about/) - A summary of what DOG is.
*  [Manual](/manual/) - The manual of all the DOG commands.
*  [Download](/download/) - The current and historical downloads.
*  [Install](/install/) - An install guide
*  [License](/license/) - A copy of GNU GPL v2 the license DOG is distributed under.
*  [All posts](/post/) - Here you'll find blog-style posts about DOG and the code.
   I plan to write about the code and what I discover and learn while implementing dog.
   All release notes are also posts.
