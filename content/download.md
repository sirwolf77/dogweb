---
title: "Downloads"
date: 2024-11-25T08:34:03+01:00
description: "DOG Downloads"
Author: Wolf Bergenheim
aliases: ["archive"]
---

## Latest version of DOG is 0.8.5b

*  [dog085bs.zip](/downloads/dog085bs.zip) - This is the latest source release
*  [dog085bx.zip](/downloads/dog085bx.zip) - This is the latest binary release

## Older versions

Here are some older versions of DOG for archival purposes.

*  0.8.4b
   *  [dog084bs.zip](/downloads/dog084bs.zip) - Source code for DOG 0.8.4b
   *  [dog084bx.zip](/downloads/dog084bx.zip) - Binaries for DOG 0.8.4b
*  0.8.3b
   *  [dog083bs.zip](/downloads/dog083bs.zip) - Source code for DOG 0.8.3b
   *  [dog083bx.zip](/downloads/dog083bx.zip) - Binaries for DOG 0.8.3b
*  0.8.2b
   *  [dog082bs.zip](/downloads/dog082bs.zip) - Source code for DOG 0.8.2b
   *  [dog082bx.zip](/downloads/dog082bx.zip) - Binaries for DOG 0.8.2b
*  0.8.1b
   *  [dog081bs.zip](/downloads/dog081bs.zip) - Source code for DOG 0.8.1b
   *  [dog081bx.zip](/downloads/dog081bx.zip) - Binaries for DOG 0.8.1b
*  0.73
   *  [dog073s.zip](/downloads/dog073s.zip) - Source code for DOG 0.73
*  0.72
   *  [dog072s.zip](/downloads/dog072s.zip) - Source code for DOG 0.72
