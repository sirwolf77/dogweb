---
title: "Install"
date: 2024-06-16T17:29:46+02:00
description: "How to install DOG"
Author: Wolf Bergenheim
aliases: []
---

It's easy to install DOG. All you need is an unzip, like `UNZIP.EXE`
shipped with [FreeDOS](https://freedos.org).

### Step One

Grab the latest binary [dog085bx.zip](/downloads/dog085bx.zip) and
save it somewhere.

### Step Two

Extract your zip file with the command `unzip -d C:\ dog084bx.zip`.
This will create a directory structure like this:

```
dog
├── bin
│   ├── dog2.ico
│   ├── dog.ico
│   ├── bp.com
│   ├── br.com
│   ├── cl.com
│   ├── cm.com
│   ├── cp.com
│   ├── dog.com
│   ├── dog.dog
│   ├── ds.com
│   ├── dt.com
│   ├── generr.com
│   ├── hd.com
│   ├── hh.com
│   ├── ls.com
│   ├── msdos.dog
│   ├── mv.com
│   ├── pp.dog
│   ├── pt.dog
│   ├── rm.com
│   ├── rt.com
│   ├── sz.com
│   ├── tp.com
│   ├── vf.com
│   ├── vr.com
│   └── wi.com
└── doc
    ├── ggpl.txt
    ├── manual.txt
    ├── readme_x.txt
    ├── rn080b.txt
    ├── rn081b.txt
    ├── rn082b.txt
    ├── rn083b.txt
    ├── rn084b.txt
    └── rn085b.txt
```

### Step Three

Modify your `config.sys` or `fdconfig.sys` file to set DOG as your `SHELL`.
Replace any existing `SHELL=` line with the following:

```
SHELL=C:\DOG\BIN\DOG.COM -P -E:1024 -A:1024
```

This will make DOG your default `SHELL` or command interpreter.

### Step Four

Copy the `dog.dog` file from `C:\DOG\BIN` to `C:\` and edit it to suit
your needs.

### Step Five

Reboot your computer, and start using DOG!
