---
title: "Manual"
date: 2024-06-16T17:29:40+02:00
description: "DOG Operation Guide"
Author: Wolf Bergenheim
Aliases: ["readme", "man", "doc", "documentation"]
---

## Introduction

When working with DOG in the prompt, you typically use the internal
and external commands, and other programs.

The batch commands are used to program batch files, called DOGfiles,
and cover things like loops, forks and user input. They aren't available
when working on the prompt.

The external commands are stand-alone executables, but considered
to be part of DOG, and may access DOG internals where needed.

### Syntax

```
DOG [-P|-E envsz|-A aliassz|-C command line]
```

#### Parameters

*  `-P` - Makes DOG a PERMANENT shell.
*  `-E:envsz` - Makes the environment to envsz bytes (divisible by 16).
*  `-A:aliassz` - Makes the alias space to aliassz bytes (divisible by 16).
*  `-C command line` - Executes the command line and exits.

## Internal Commands

These internal commands are built into to the DOG binary and are
available at the command prompt as well as in DOGfiles (see below).

### AL - Alias

#### Syntax

```
AL [ALIAS] [COMMAND]
```

##### Parameters

*  `ALIAS`   - The command alias to set/unset The name is UPPERCASED.
*  `COMMAND` - The real command that ALIAS expands to. Without COMMAND, AL unsets ALIAS
*  - Without arguments the command prints all ALIASes to the screen.

#### Effect

Creates a command ALIAS

#### Example

`AL CV CP -v` creates an alias command `CV` which expands into the command `CP -v`.

You can also use AL to create an ALIAS of an existing command. It has
the same effect as if you typed the expansion.
e.g. `AL CP CP -v` makes it so that whenever you use just
`CP`, the `-v` option is used, handy for setting default options.


### CC - Change Codepage [NOT IMPLEMENTED YET]

#### Syntax

```
CC [CODEPAGE]
```

##### Parameters

*  `CODEPAGE` - Codepage number to set as current
*  cc without Args shows available codepages.

#### Effect

Changes the current codepage to CodePage.

### CD - Change Directory

#### Syntax

```
CD [PATH]
```

##### Parameters

*  `PATH` - The destination path.
   The `..` directory can be appended directly to cd like in `COMMAND.COM`.
   e.g. `cd..`
   The virtual directory `...` means `..\..` e.g. `cd...` is equal to `cd ..\..`.
*  cd without Args prints the current directory on the screen.

#### Effect

Changes the current directory to Path.

#### Note

In DOG, directories are executable: typing `dog\src\util` implicitly means
`cd dog\src\util` when `util` is a directory.

### CT - Change Terminal

#### Syntax

```
CT DEV
```

##### Parameters

*  `DEV` - The name of the device that I/O is set to. Eg. `CON`, `AUX`, `NUL`

#### Effect

Changes the current IO terminal to Device.

### EH - Echo

#### Syntax

```
EH [OPTION] [TEXT]
```

##### Parameters

* `TEXT` - Anything you want to type out from a DOGfile.
* EH by itself will put a new line.

`OPTION` can be one of these (in any order):
*  `-n` - Do not format the text with the special characters

Use these special characters to format the text:
*  `$$` - the $ sign.
*  `$_` - space
*  `$b` - vertical bar ( � )
*  `$e` - The ESC character (ASCII 27 = 1b in hex)
*  `$l` - Left angle ( < )
*  `$g` - Right angle ( > )
*  `$n` - New line
*  `$r` - Carriage return
*  `$t` - Tabulator

#### Effect

Print a (formatted) string to stdout (normally the screen).

### MD - Make Directory

#### Syntax

```
MD <DIR>
```

##### Parameters
*  `DIR` - Name or Path of new directory.

`OPTION` can be one of these:
*  `-p` - Create the needed directories in the path

#### Effect

Creates a new directory named `DIR`.

### RD - Remove Directory

#### Syntax

```
RD <DIR>
```

##### Parameters

*  `DIR` - Name or path of an **empty** directory to remove.

#### Effect

Removes directory named `DIR`

### SE - Set Environment

#### Syntax

```
SE [VARIABLE] [VALUE]
```

##### Parameters

*  `VARIABLE` - the name of the environment variable. The name is UPPERCASED.
*  `VALUE` - The value that `VARIABLE` should have. Without `VALUE`, SE unsets `VARIABLE`
*  Without arguments the command prints all VARIABLEs to the screen.

Variables set and used by DOG:

*  `COMSPEC` - Path to DOG
*  `PATH` - The list of paths where to search for commands.
   The paths are separated by the `;` character.
*  `PROMPT` - The string displayed indicating that DOG is waiting for a command
   You can use these special characters:
   *  `$$` - the $ sign.
   *  `$_` - space
   *  `$b` - vertical bar `|`
   *  `$e` - The ESC character (ASCII 27 = 1b in hex)
   *  `$l` - Left angle `<`
   *  `$g` - Right angle `>`
   *  `$n` - New line
   *  `$r` - Carriage return
   *  `$t` - Tabulator
   *  `$p` - Current drive and path
   *  `$c` - Current time

#### Effect

Sets the specified VARIABLE to VALUE in the environment.

### TN - TRUENAME

#### Syntax

```
TN <PATH>
```

##### Parameters:

*  `PATH` - Name or path of to resolve.

##### Effect

Returns the 'true name' of the given input by resolving *, . and .. entries and
also any SUBST, ASSIGN and JOIN are unravelled

### XX - Exit

#### Syntax

```
XX
```

#### Effect

Exit the current DOG shell, unless it is the primary shell,
in which case the command has no effect.

## External commands

These commands are actually external stand-alone programs and can be
used outside of DOG.

### BP - Beep

#### Syntax

```
BP [FREQ [TIME]]
```

##### Parameters
*  `FREQ` - Frequency of the sound in Herz (Hz)
*  `TIME` - Duration in milliseconds (1000 ms = 1 second)
*  Without parameters, BP puts ASCII character 07h into stdout,
   causing a beep provided that the ANSI driver is loaded.

#### Effect

Makes a constant sound with frequency FREQ  Hz for TIME milliseconds
through the PC Speaker.

Some Music notes and their frequency:

*  C ~ 262 Hz
*  D ~ 294 Hz
*  E ~ 330 Hz
*  F ~ 349 Hz
*  G ~ 392 Hz
*  A = 440 Hz
*  H/B ~ 493 Hz
*  C ~ 523 Hz

### BR - Break

#### Syntax

```
BR [ON|OFF]
```

##### Parameters

*  `ON` - Set DOS Break flag to ON: Check for break on all DOS calls.
*  `OFF` - Set DOS Break flag to OFF: Check for break only when reading/writing
   stdin/stdout/stderr.

#### Effect

Toggles the DOS Break flag. In case no parameters are given it returns the Break Flag

### CL - Clear screen

#### Syntax

```
CL
```

#### Effect

Clears the screen. Works on 50 lines/page screens too.

### CM - CHMOD, change file attributes

#### Syntax

```
CM [OPTION]... <ATTRIBUTES> <PATTERN|@FILE>...
```

##### Parameters

*  `ATTRIBUTES` - `ATTRIBUTES` should be preceded by a +,- or = character
   indicating whether to add, remove or set ATTRIBUTES.
   Then the list of attributes is given (in any order):
   *  `A` - Archive
   *  `S` - System
   *  `R` - Read-only
   *  `H` - Hidden
*  `PATTERN` - Pattern can be a number of paths including wild characters `*` and `?`
   *  `?` represents any single character
   *  `*` represents 0 or more characters e.g. `*.dog` `*.*` `DO?.COM`, `*.?`
*  `@FILE` - Filename preceded by '@' which is read and every line is
   handled like a `PATTERN`

`OPTION` can be one of these (in any order):

*  `-i` - prompt (Yes/No/All) for each file or directory
*  `-a` - act on read-only, system and hidden files
* `-r` - recurse into sub-directories. Sets ATTRIBUTES for all files
   and directories too
*  `-v` - print filename of each modified file

#### Effect

Changes `ATTRIBUTES` of the file(s) and/or directory(ies) matching
`PATTERN`

### CP - Copy

#### Syntax

```
CP [OPTION]... SOURCE DEST
```
or
```
CP [OPTION]... [PATH1\]PATTERN1 [PATH2\]PATTERN2
```

##### Parameters

*  `SOURCE` - The source file(s) to copy
*  `DEST` - The target file(s) to create

or

*  `PATTERN1` - The source file(s) to copy
*  `PATTERN2` - The target file(s) to create

<br>

`OPTION` can be one of these (in any order):
   *  `-v` - print filename of each copied file
   *  `-f` - force all files to be overwritten
   *  `-i` - interactive, asks before copying every file

#### Effect

Copies one or more files to the destination. CP asks before
overwriting a file (can be overridden with -f).

#### Examples

```
cp dog.com wolf.com
cp dog.com c:dog
cp dog.com c:dogwolf.com
cp c:dog
cp dog.* wolf.*
cp dog.* wolf.*
cp *.o *.obj
```

### DS - Directory stack

#### Syntax

```
DS [SUBCOMMAND]
```

`SUBCOMMAND` can be one of the following:
*  `push` `[PATH]`: Pushes the current directory on to the Directory Stack
   If the optional absolute or relative path are given it also changes to that directory
*  `pop`: Pops a directory off the stack and changes to that directory
*  `clear`: Clears the Directory Stack
*  `list`: Lists the entire Directory Stack

#### Effect

Manages a directory stack. You can push and pop directories to and from the
stack.  You can also clear and list the stack.

### DT - Date and Time

#### Syntax

```
DT [OPTION]
```

##### Parameters

*  `OPTION` can be one of these:
   *  `-s` - set DATE and TIME
   *  `-d` - set DATE only
   *  `-t` - set TIME only

#### Effect

DT displays the current date and time. By using the options `DT` can
be used to set the DATE and/or TIME

### HD - Head

#### Syntax

```
HD [OPTION] FILE
```

##### Parameters

`OPTION` can be one of these:

*  `-c <NUM>` - Number of characters from the start to display
*  `-n <NUM>` - Number of lines from the start to display
*  `-h` - Display help

FILE is the path to a file to display
Effect: HD Displays the top of a (text) file.
        By default 10 lines are printed.

### HH - Help

#### Syntax

```
HH [DOG | ALL | COMMAND]
```

##### Parameters

* `DOG` - Gives syntax for DOG.
* ` ALL` - Prints out the full DOG manual.
* `COMMAND` - Giving the name of a DOG command will display detailed help for
   that command.

HH alone starts the DOG introduction.

#### Effect

Displays usage information for DOG commands.

### LS - List directory

#### Syntax

```
LS [OPTION]... [PATTERN]...
```

##### Parameters

`PATTERN` - Pattern can be a number of paths including wild characters `*` and `?`
   *  `?` represents any single character and
   *  `*` represents 0 or more characters
   e.g. `*.dog` `*.*` `DO?.COM,` `*.?`

`OPTION` can be one of these (in any order, some don`t work together):
   *  `-c` - Use ANSI color to color entries. See `COLORS` below for format
   *  `-d` - Directories Only
   *  `-f` - Files Only
   *  `-l` - show volume label
   *  `-p` - pause at every screen full or `PATTERN`
   *  `-s` - sort output. See SORTING below for format
   *  `-w` - show file names only (6 per row)
   *  `-x` - show sum of file sizes and count of files and dirs
   *  `-z` - show human readable sizes (B, KB, MB, GB)

`COLORS`

**NOTE**: Requires `NANSI.SYS` or equivalent ANSI driver to work

Colors are taken from the `LSCOLORS` env variable and is a `CSV` of
color `RULES`. A `RULE` is structured as `EXT=FG;BG;ATTR` where:

*  EXT is a file extension or a special one for matching on file attributes:
   *  `_D` - Directory
   *  `_S` - System file
   *  `_L` - Label
   *  `_H` - Hidden file
   *  `_A` - Archive
*  FG is the foreground color, one of:
   *  `30` - Black
   *  `31` - Red
   *  `32` - Green
   *  `33` - Yellow
   *  `34` - Blue
   *  `35` - Magenta
   *  `36` - Cyan
   *  `37` - White
*  BG is the background color, one of:
   *  `40` - Black
   *  `41` - Red
   *  `42` - Green
   *  `43` - Yellow
   *  `44` - Blue
   *  `45` - Magenta
   *  `46` - Cyan
   *  `47` - White
*  ATTR sets additionnal video attributes:
   *  `0` - Resets attributes and colors to white on black
   *  `1` - Bold/light color
   *  `4` - Underlined text
   *  `5` - Blinking text
   *  `7` - Reverse Video

Default rules if `LSCOLORS` is unset:

```
_D=34;0;1,_H=30;40;8,_R=33;40,_S=31;40;1,_L=32;40,EXE=35;40,COM=35;40,DOG=35;40
```

`SORTING`

Sorting format is a single string of the following format:

`-s[SORTORDER]...` - Sort the result with the given rules `SORTORDER`
can be any of these letters:

A `+` prefix means ascending, `-` prefix means descending

*  `n` = sort directories before files
*  `s` = sort by file size
*  `a` = sort by attribute: Archive, Directory, Hidden, Label, Read-Only, System
*  `d` = sort by date
*  `t` = sort by time
*  `f` = sort by file name
*  `e` = sort by file extension

Example: `-s-d-s` sorts by date (newest first) and then file size
(largest first)

Example: `-s-s+e+f` sorts by size (largest first) and then by
extension and then name (alphapetically)

The default `SORTORDER` is `+n+f` meaning that directories are grouped
first and then sorted by name followed by files sorted by name
(alphabetically).

#### Effect

List files according to switches and search `PATTERN`.


### MV - Move / Rename File

#### Syntax

```
MV [OPTION]... SOURCE DEST
```
or
```
MV [OPTION]... [PATH1\]PATTERN1 [PATH2\]PATTERN2
```

##### Parameters

*  `SOURCE` - The source file to move
*  `DEST` - The new file name or target

or

*  `PATTERN1` - The source file name(s)
*  `PATTERN2` - The new file name(s)

`OPTION` can be one of these (in any order):
   *  `-v` - print filename of each moved file

#### Effect

Moves one or more files to the destination or renames files according to the patterns.
MV asks before overwriting an existing file.

#### Examples

```
mv dog.com wolf.com
mv dog.com c:dog
mv dog.com c:dogwolf.com
mv c:dog
mv dog.* wolf.*
mv dog.* wolf.*
mv *.o *.obj
```
### PP - Prompt

#### Syntax

```
PP [PROMPT]
```

##### Parameters

PP alone displays the formatting for the current DOG prompt.

*  `PROMPT` - The string displayed indicating that DOG is waiting for a command
   You can use these special characters:
   *   `$$` - the $ sign.
   *   `$_` - space
   *   `$b` - vertical bar `|`
   *   `$e` - The ESC character (ASCII 27 = 1b in hex)
   *   `$l` - Left angle `<`
   *   `$g` - Right angle `>`
   *   `$n` - New line
   *   `$r` - Carriage return
   *   `$t` - Tabulator
   *   `$p` - Current drive and path
   *   `$c` - Current time

#### Effect

Displays the PROMPT or sets the PROMPT.

#### Examples

```
pp  $n$_$_$b\_$n$_$_$b$_.\---.$n$_/$_$_$_,__/$n/$_$_$_/$p$_%%
```

Results in this propmt:

```

  |\_
  | .\---.
 /   ,__/
/   /C:\DOG %
```

### PT - Path

#### Syntax

```
PT [OPTION] [PATH]
```

##### Parameters

PT alone displays the current PATH"

*  `PATH`  - The list of directories which DOG uses to find programs
   to run. The PATH is a list of directories separated by a `;`
   character.

`OPTION` can be one of these (in any order):

*  `-a` - Append the given PATH instead of setting it to the PATH.

#### Effect

Displays or sets the PATH which DOG uses to find programs to run.

### RM - Remove

#### Syntax

```
RM [OPTION]... <PATTERN|@FILE>...
```

##### Parameters

`PATTERN` - Pattern can be a number of paths including wild characters `*` and `?`
   *  `?` represents any single character and
   *  `*` represents 0 or more characters
      e.g. `*.dog` `*.*` `DO?.COM`, `*.?`

`@FILE` - Filename preceded by `@` which is read and every line is handled
 like a `PATTERN`

`OPTION` can be one of these (in any order):
   *  `-a` - also remove read-only, system and hidden files
   *  `-i` - prompt (Yes/No/All) for each file or directory
   *  `-r` - recurse into sub-directories. Removes all files in subdirectories too
   *  `-v` - print filename of each removed file

#### Effect

Removes the file(s) and/or directory(ies) matching `PATTERN`

### RT - Remove Tree

#### Syntax

```
RT [OPTION]... <DIRECTORY|@FILE>...
```

##### Parameters

*  `DIRECTORY` - The directory(s) to delete.
*  `@FILE` - Filename preceded by `@` which is read and every line is handled
   like a DIRECTORY
*  `OPTION` can be one of these (in any order):
   *  `-a` - also remove read-only, system and hidden files
   *  `-i` - prompt (Yes/No/All) for each file or directory
   *  `-v` - print filename of each removed file

#### Effect

Removes the tree(s) indicated by DIRECTORY(s) with all their subdirectories and files.

### SZ - calculate file Sizes

#### Syntax

```
SZ [OPTION]... [PATTERN]...
```

##### Parameters

*  `PATTERN` - Pattern can be a number of paths including wild characters `*` and `?`
   *  `?` represents any single character and
   *  `*` represents 0 or more characters
      e.g. `*.dog` `*.*` `DO?.COM`, `*.?`

*  OPTION can be one of these (in any order):
   *  `-k` - give sizes in KB
   *  `-m` - give sizes in MB
   *  `-r` - recurse into sub-directories
   *  `-z` - show human readable sizes (B, KB, MB, GB)

#### Effect

Prints the size of the files matching `PATTERN`.

### TP - Type File contents

#### Syntax

```
TP FILE...
```

##### Parameters

*  `FILE` - A space separated list of files

#### Effect

Types the contents of the files specified by `FILE` to stdout.

### VF - DOS Verify flag

#### Syntax

```
VF [ON|OFF]
```

##### Parameters

*  `ON` - Set the DOS Verify flag to ON
*  `OFF` - Set the DOS Verify flag to OFF

#### Effect

If ON or OFF specified sets Verify flag to this value.
Otherwise VF returns the current setting.

### VR

#### Syntax

```
VR
```

#### Effect

Prints the DOG and DOS version.
VF tries very hard to identify the flavor of DOS.

### WI - Which command

#### Syntax

```
WI [OPTION]... COMMAND...
```

##### Parameters

*  `COMMAND` is a command to look for.
*  `OPTION` can be one of these (in any order):
   *  `-a`     - Use the APPEND environment variable instead of PATH
   *  `-e ENV` - Use the ENV environment variable instead of PATH
   *  `-f`     - Print Full file info (date, size and attributes)
   *  `-x`     - Look for DOS .BAT files instead of DOGfiles (.DOG)

#### Effect

Shows which program is run with a command.  WI looks for programs in
the current directory and then in each directory of the `PATH`.

## DOGfile commands

These commands are only available when running DOGfiles. Dogfiles have
a `.dog` file ending.  Note that unlike `COMMAND.COM`, DOGfiles are
always the equivalent of `ECHO OFF`, that is the commands are not
echoed to STDOUT when a DOGfile is being processes.

### CA - Call other DOGfile

#### Syntax

```
CA <DOGFILE>
```

##### Parameters

*  `DOGFILE` - A DOGfile to call

#### Effect

Calls the DOGFILE from another. Once it has finished running, execution
continues on the next line in the DOGfile where CA was executed from.

### DO - Do command while condition is true

#### Syntax

```
DO <COMMAND> [WHILE <CONDITION>]
```

##### Parameters
*  `COMMAND`   - The command to repeat.
*  `CONDITION` - The condition for repeating the command.

CONDITION can take the form:

*  `ERROR [IS|NOT] <NUMBER>` - `NUMBER` compared to the ERRORLEVEL
*  `<VARIABLE> [IS|NOT] <VALUE>` - `VARIABLE` is an environment variable
   and is compared to `VALUE`
*  `[NOT] EXIST <FILE>` - Returns true as long as `FILE` exists.

<br>

*  `IS` - is optional and has no effect other than makes the statement
   look a little bit more like English.
*  `NOT` - reverses the condition value in all cases.

#### Effect

Repeats `COMMAND` while `CONDITION` is true. Without a `CONDITION` the
`COMMAND` is repeated as long as it returns 0 as its exit code.


### 44 - For loop

#### Syntax

```
44 <VAR> IN <SET> DO <COMMAND>
```

##### Parameters
*  `VAR` - A variable name, use as %%VAR%% as a placeholder in COMMAND
*  `SET` - A comma separated list of words.
*  `COMMAND` - The command to repeat.
   You can use %%VAR%% as a placeholder for VAR.

#### Effect

For each word in `SET` runs the `COMMAND`.  Each word in SET will be
set to VAR for each repeat of the for loop.  This allows the `COMMAND`
to include `%%VAR%%` which will be replaced with a word from `SET`.

#### Example

```
44 F IN DOG,CAT,WOLF,BEAR DO MV %%F%%.CC %%F%%.CPP
```

This command will first run `MV DOG.CC DOG.CPP` by replacing `%%F%%`
with `DOG` in the first iteration, etc for each of the terms in SET.


### GO - Go to label

#### Syntax

```
GO <LABEL>
```

##### Parameters

*  `LABEL` - A label in the DOGfile.

#### Effect

Jumps the DOGfile processing to the LABEL. A label is identified as
line containing `:LABEL`. DO will jump to the line after the `:LABEL`
line.

### IF - Conditional execution

#### Syntax

```
IF CONDITION <COMMAND1> [ELSE <COMMAND2>]
```

##### Parameters

*  `COMMAND1`  - The command to run if CONDITION is TRUE.
*  `COMMAND2`  - The optional command to run if CONDITION is FALSE.
*  `CONDITION` - The condition for repeating the command.

CONDITION can take the form:

*  `ERROR [IS|NOT] <NUMBER>` - `NUMBER` compared to the `ERRORLEVEL`
*  `<VARIABLE> [IS|NOT] <VALUE>` - `VARIABLE` is an environment variable
   and is compared to `VALUE`
*  `[NOT] EXIST <FILE>` - Returns true as long as FILE exists.

<br>

*  `IS`  - is optional and has no effect other than makes the statement
   look a little bit more like English.
*  `NOT` - reverses the condition value in all cases.

#### Effect

If `CONDITION` is `TRUE` the `COMMAND1` is executed, else `COMMAND2` is executed.

### IN - Input

#### Syntax

```
IN <VARIABLE> [PROMPT]
```

##### Parameters

*  `VARIABLE` - The environment variable to store the user input.
*  `PROMPT` - An optional message to display to the user.
   With no prompt DOG will display `Enter value for VARIABLE: `

#### Effect

Prompts the user for input and saves that input in the environment.

### SH - Shift

#### Syntax

```
SH [NUM]
```

##### Parameters

*  `NUM` - A number to indicate how many variables to shift

#### Effect

Shifts the DOGfile input arguments by NUM steps. Normally A DOGfile
can only index 10 arguments at one time (%0 to %9). This command
allows a DOGfile to shift the variables right so that what was
previously %1 is now %2, etc.

By giving the command a number `NUM`, the arguments are shifted
`NUM` steps instead of just one to a maximum of 9.

### TI - Timed break

#### Syntax

```
TI [TIME]
```

##### Parameters

*  `TIME` - The number of seconds to wait.
*  Without an argument `TI` waits forever for a key stroke

#### Effect

Waits TIME seconds or for a key press before continuing the DOGFile
execution
